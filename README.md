# README #

Bitbucket Repository for Deep Learning final project

GAN\_DCGAN.ipynb has basic implementations of GAN and DCGAN architectures for MNIST dataset.

DCGAN networks are not all convolutional at this point. Generator has deconvolution layers (convolution transpose).
Discriminator is still a dense network making use of the same one in GAN model.

CycleGAN is a very basic implementation. It is made of conv\_2d and conv\_2d\_transpose. There is no normalization,
resnet, or anything else yet. Not really sure if it is even working right.

Used these as references to implement CycleGAN:
	(1) https://github.com/XHUJOY/CycleGAN-tensorflow
	(2) https://github.com/LynnHo/CycleGAN-Tensorflow-Simple
	
Used this link to work on basic GAN:
	(1) https://medium.com/towards-data-science/gan-introduction-and-implementation-part1-implement-a-simple-gan-in-tf-for-mnist-handwritten-de00a759ae5c

For GAN and DCGAN MNIST dataset that comes with tensorflow was used. The generated images are reasonable.

For CycleGAN apple2orange dataset was used. Unable to generate anything meaningful at this point. Have to continue on this.
### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact